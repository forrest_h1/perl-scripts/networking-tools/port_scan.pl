#!/usr/bin/perl
#Written by Forrest Hooker, 02/27/2023
#Simple utility to scan a host for open, ACCESSIBLE ports

## Pragma ##
use strict;
use warnings;
use diagnostics;
use 5.010;

## Functional pragma ##

#Used for testing if host given is up
use Net::Ping;
#Actual port scanning module
use IO::Socket;
#Used to validate IP address in __arg_check__();
use Data::Validate::IP;
#Used for colored output on shell.
use Term::ANSIColor qw(:constants);
#Used for bash-like array checks without using grep.
use experimental 'smartmatch';

#Flush print buffer immediately
$| = 1;

## Vars/Arrs ##

#EMPTIES#

#Define $error_code as empty, is only filled if script hits an issue.
my $error_code;
#Define $socket as empty, later filled with IO::Socket test function
my $port_test;

#POPULATED#

#Define $target_host as user argument 
my $target_host = $ARGV[0];

#Define %ports_arr as list of common/uncommon ports (would like to just read from a file eventually)
my %ports_arr = (
	22, "SSH",
	23, "Telnet",
	25, "SMTP",
	49, "TACACS Login",
	80, "HTTP",
	161, "SNMP",
	162, "SNMP Trap",
	177, "XDMCP",
	194, "IRC",
	280, "HTTP Mgmt.",
	443, "HTTP/S",
	554, "RTSP",
	#Both VMWare ports might be wrong
	902, "VMWare ESXI",
	903, "VMWare ESXI Server Agent",
	992, "Telnet over TLS/SSL",
	3389, "Remote Desktop Protocol",
	4444, "I2P HTTP/S Proxy",
	5900, "VNC Server",
	6516, "Windows Admin Server",
	8000, "Python/Django Server",
	8006, "ProxMox Admin Web Interface",
	8008, "Alternate HTTP (Sometimes IBM)",
	8080, "Alternate HTTP (Apache, JIRA, Asterisk",
	8090, "Confluence",
	8123, "Home Assistant",
	8443, "Apache Tomcat SSL",
	8767, "TeamSpeak Voice Channel",
	9151, "Tor Browser",
	10500, "Zabbix Agent",
	10051, "Zabbix Client Trap",
	32764, "Backdoor for certain network devices",
); #End %ports_arr array

#Array of possible user flags that would invoke the __usage__ sub-routine
my @help_flags = (
	"help",
	"HELP",
	"Help",
	"usage",
	"USAGE",
	"Usage",
	"--usage",
	"-h",
	"-H",
	"--h",
	"--H",
); #End @help_flags array

#COMMAND VARS#

#Define $ip_check as Data::Validate's IP Test function
my $ip_check = Data::Validate::IP->new;

#Define $target_host_ping as the ping function
my $target_host_ping = Net::Ping->new();


## Main Sub-Routines ##


#Main Sub-routine, calls all other defined sub-routines
sub __main__ {
	#Argument Check sub-routine
	__arg_check__();
	#Target Host Ping Check sub-routine
	__target_check__();
	#Port Scan sub-routine
	__port_scan__();
} #end __main__ sub-routine


#Argument Check Sub-routine - reads all user input and proceeds accordingly
sub __arg_check__(){
	#If the user has given an arg matching any of the @help_flags...
	if ( $target_host ~~ @help_flags){
		#Run the __usage__ sub-routine
		__usage__();
	#Else, if no argument has been given for $target_host...
        }elsif ( ! defined $target_host){
		#Notify user that argument is empty
		print RED, BOLD, "\nNo host given. Please run give an IP address as an argument.", RESET;
		#Give user simple example
		print YELLOW, "\nEX: ./port_scan.pl 192.168.1.1\n", RESET;
		#Set $error_code to "Empty Argument"
		$error_code="Empty Argument";
		#Run __error_exit__ sub-routine.
		__error_exit__();
	#Else, if the IP Address given is not a valid IP address...
	}elsif ( ! $ip_check->is_ipv4($target_host)){
		#Notify user of conditions for script
		print RED, BOLD, "\nInvalid IP Address. This script only supports IPv4 addresses, not IPv6 or DNS entries.\n", RESET;
		#Set error code to "Invalid IP Address"
		$error_code="Invalid IP Address";
		#Run __error_exit__ sub-routine
		__error_exit__();
       } #End if statement	       
} #End __arg_check__ sub-routine


#Sub-routine to check if $target_host is reachable or not.
sub __target_check__(){
	#Notify user that host will be pinged...
	print WHITE, "\nTesting if $target_host is reachable...\r", RESET;
	#If $target_host_ping is unsuccessful...
	if ( ! $target_host_ping->ping($target_host)){
		#Notify user that given $target_host can't be reached (Make this more verbose!)
		print RED, BOLD, "$target_host is unreachable via TCP packet.\r", RESET;
		#Set error code to "Unreachable host"
		$error_code="Unreachable host";
		#Run __error_exit__ sub-routine
		__error_exit__();
	#Else, if $target_host_ping IS successful...
	}else{
		#Notify user that host is up and the scan will start. 
		print GREEN, BOLD, "\r$target_host is reachable.                   \n\n", RESET;
	}
} #End __target_check__ sub-routine

sub __port_scan__(){
	#For each port number in %ports_arr that has been sorted numerically...
	foreach my $ports_num (sort {$a <=> $b} keys %ports_arr){
		#Notify user of each port being scanned
		print WHITE, "Scanning port $ports_num\r", RESET;
		#Define $read_out such that it appears like this: [80 - (HTTP)]
		my $read_out = "$ports_num - ($ports_arr{$ports_num})";
		#Connect to target host socket
		$port_test = IO::Socket::INET->new(PeerAddr => $target_host , PeerPort => $ports_num , Proto => 'tcp' , Timeout => 1);
		#If $port_test function is successful for the port, then...
		if ( $port_test ){
			#Notify user that this port is there and give them info on what it is
			print GREEN,"Port :$read_out is open on $target_host\n", RESET;
		#Else, if $port_test is UNsuccessful, then...
		}else{
			#Use my ugly method of clearing lines that are using "\r"
			print("                             \r");
		} #End foreach loop
	} #End If statement
	#Notify user that Port scan is completed.
	print WHITE, BOLD, "\nPort scan of $target_host completed.\n\n", RESET;
	#Exit script.
	exit();
} #End __port_scan__ sub-routine


## Help & Exit Sub-Routines ##

#Sub-routine used to exit script cleanly and notify user if there was a problem.
sub __error_exit__(){
	#Put something that spits out and defines errors here#
	print RED, "\nError Code: ", BOLD, UNDERLINE, "$error_code\n", RESET;
	#Notify user of exit (Maybe make codes affect colors)
	print YELLOW, "\nQuitting...\n\n", RESET;
	#Exit the script
	exit();
} #End __error_exit__ sub-routine

#Sub-routine just for simple usage print-out
sub __usage__(){
	print WHITE, BOLD, UNDERLINE, "\nport_scan.pl", RESET; 
	print WHITE, "\nport_scan.pl currently only supports IPv4 Addresses and a single protocol. This will eventually be changed.\n", RESET;
	print WHITE, "To scan a target for open ports, run ./port_scan.pl with a valid IPv4 address. ex: 192.168.1.1\n", RESET;
	print WHITE, BOLD, "EX: ./portscan.pl 192.168.1.1\n", RESET;
	print WHITE, "If the host given is able to be pinged, the script will then test for each port.\n\n", RESET;
	exit();
} #End __usage__ sub-routine


######


__main__();
	

