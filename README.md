# port_scan.pl


port_scan.pl is a single host open port scanner written in Perl using the IO::Sockets, Net::Ping, & Data::Validation modules. It is presently only working with specific TCP ports and ONLY IPv4 addresses (I would like to add IPv6 soon). This is the 2nd script I've ever written in Perl, so there might be some weirdness to folk very experienced with this language :)


## Required Modules


- IO::Sockets (Used for actual port scan functionality)


- Net::Ping (Used to test if host is up or down)


- Data::Validation::IP (Used to check user argument for a valid IPv4 Address)


- Term::ANSIColor (Used for purty colors)


## How Does It Work?


port_scan.pl must be given a singular IPv4 Address, like `192.168.1.100`. Upon being given an IP, the script will run the `Data::Validate::IP` function to see if it's been given a proper IPv4 address. If it has, the script will then ping it for reachability using the `Net::Ping` function. If the host is reachable, the program will then initiate a port scan using the `IO::Socket` function, which I have set to use TCP and a timeout of 1 (This will later be able to be user-defined). As each port is found, the port number, along with its common usage, will be printed out.

Bear in mind: I wrote this extremely quick, so v1 is pretty sloppy in some of its implementation :) (Ex: Every port is being tested with a TCP packet)


## Usage


To run port_scan.pl, simply run the following command: `./port_scan.pl [x.x.x.x]`, replacing [x.x.x.x] with a functional IPv4 Address; like 192.168.1.1.


## Todo


- Add IPv6 Functionality


- Add option for user-defined protocols and timeouts


- Organize the ports array better (i.e. separate TCP ports from UDP ports, etc.)


- Add more ports commonly used in a Red-Team/SysAdmin role

